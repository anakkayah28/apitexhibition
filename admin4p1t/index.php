<?php
session_start();
include_once "library/inc.connection.php";
include_once "library/inc.library.php";
include_once "library/inc.pilihan.php";
include_once "library/inc.tanggal.php";
include_once "library/config.php";

// Baca Jam pada Komputer
date_default_timezone_set("Asia/Jakarta");
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<html lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Apit</title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Bootstrap Core CSS -->
    <link href="./bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="./bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

     <!-- DataTables CSS -->
    <link href="./bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="./bower_components/datatables-responsive/css/responsive.dataTables.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom CSS -->
    <link href="./dist/css/sb-admin-2.css" rel="stylesheet">


    <!-- Custom Fonts -->
    <link href="./bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
   
 
    <link rel="stylesheet" href="./dist/css/select2.min.css"/>
    <script type="text/javascript" src="./plugins/tigra_calendar/tcal.js"></script> 
    <link rel="stylesheet" type="text/css" href="./plugins/tigra_calendar/tcal.css" />
    <link href="./styles/style.css" rel="stylesheet" type="text/css">
    <link href="./bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body style="background-color: #ffffff;" >
<div id="wrapper">
    <!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" style="background-color: #ffffff;border-color:#ffffff;" role="navigation" style="margin-bottom: 0"> 
    <div class="navbar-header">
    <a class="navbar-brand" href="index.html"><img src="./images/LOGO.jpg" width="240px" class="img-fluid"></a>
    </div>
    <div style="display: flex; justify-content: flex-end; color:blue;margin-top:70px;"><h4><?php if(isset($_SESSION['SES_NMPetugas'])){echo "Login : ";echo $_SESSION['SES_NMPetugas'];echo "&nbsp;&nbsp;&nbsp;<a href='?page=Logout' ><strong><i class='fa fa-sign-out'></i>Logout</strong>&nbsp;</a>";}  ?></h4> &nbsp;  </div>
            <!-- /.navbar-header -->
    <?php include "menu.php";?>
</nav>
        <div id="page-wrapper" style="min-height: 414px;border-left:0px;">
<?php 

// function logErr($data){
//     // $logPath = __DIR__. "/../logs/logs.txt";
//     $logPath = __DIR__. "../logs.txt";
//     $mode = (!file_exists($logPath)) ? 'w':'a';
//     $logfile = fopen($logPath, $mode);
//     fwrite($logfile, "\r\n". $data);
//     fclose($logfile);
//   }

include "buka_file.php";?>


        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->

<div id="myDrop"></div>

    <script src="./js/jquery-2.1.4.min.js"></script>
    <script src="./dist/js/select2.min.js"></script>
    
    <!-- script src="./bower_components/jquery/dist/jquery.min.js"></script --> 

    <!-- Bootstrap Core JavaScript -->
    <script src="./bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="./bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="./bower_components/raphael/raphael-min.js"></script>
    <script src="./bower_components/morrisjs/morris.min.js"></script>
    <script src="./js/morris-data.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="./dist/js/sb-admin-2.js"></script>
            <div id="morris-area-chart" hidden="true"></div>
            <div id="morris-bar-chart" hidden="true"></div>
            <div id="morris-donut-chart" hidden="true"></div></div>
</body>
</html>

<script>
		$(document).ready(function(){
			$('.reorder').on('click',function(){
				$("ul.nav").sortable({ tolerance: 'pointer' });
				$('.reorder').html('Save Reordering');
				$('.reorder').attr("id","updateReorder");
				$('#reorder-msg').slideDown('');
				$('.img-link').attr("href","javascript:;");
				$('.img-link').css("cursor","move");
				$("#updateReorder").click(function( e ){
					if(!$("#updateReorder i").length){
						$(this).html('').prepend('<i class="fa fa-spin fa-spinner"></i>');
						$("ul.nav").sortable('destroy');
						$("#reorder-msg").html( "Reordering Photos - This could take a moment. Please don't navigate away from this page." ).removeClass('light_box').addClass('notice notice_error');
			 
						var h = [];
						$("ul.nav li").each(function() {  h.push($(this).attr('id').substr(9));  });
						 
						$.ajax({
							type: "POST",
							url: "ajax/update.php",
							data: {ids: " " + h + ""},
							success: function(data){
								if(data==1 || parseInt(data)==1){
									window.location.reload();
								}
							}
						}); 
						return false;
					}       
					e.preventDefault();     
				});
			});
			 
			$(function() {
			  $("#myDrop").sortable({
				items: '.dz-preview',
				cursor: 'move',
				opacity: 0.5,
				containment: '#myDrop',
				distance: 20,
				tolerance: 'pointer',
			  });
		 
			  $("#myDrop").disableSelection();
			});
			 
			//Dropzone script
			Dropzone.autoDiscover = false;
			 
			var myDropzone = new Dropzone("div#myDrop", 
			{ 
				 paramName: "files", // The name that will be used to transfer the file
				 addRemoveLinks: true,
				 uploadMultiple: true,
				 autoProcessQueue: false,
				 parallelUploads: 50,
				 maxFilesize: 5, // MB
				 acceptedFiles: ".png, .jpeg, .jpg, .gif",
				 url: "ajax/action-z.ajax.php",
			});
			 
			myDropzone.on("sending", function(file, xhr, formData) {
			  var filenames = [];
			   
			  $('.dz-preview .dz-filename').each(function() {
				filenames.push($(this).find('span').text());
			  });
			 
			  formData.append('filenames', filenames);
			});
			 
			/* Add Files Script*/
			myDropzone.on("success", function(file, message){
				$("#msg").html(message);
				//setTimeout(function(){window.location.href="index.php"},200);
			});
			  
			myDropzone.on("error", function (data) {
				 $("#msg").html('<div class="alert alert-danger">There is some thing wrong, Please try again!</div>');
			});
			  
			myDropzone.on("complete", function(file) {
				myDropzone.removeFile(file);
			});
			  
			$("#add_file").on("click",function (){
				myDropzone.processQueue();
			});
			 
		});
	</script>
  
