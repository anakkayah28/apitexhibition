<?php
# Pengaturan tanggal komputer
date_default_timezone_set("Asia/Jakarta");
function mysqli_field_name($result, $field_offset)
{
    $properties = mysqli_fetch_field_direct($result, $field_offset);
    return is_object($properties) ? $properties->name : null;
}

function mysqli_field_len($result, $field_offset) {
	$properties = mysqli_fetch_field_direct($result, $field_offset);
	return is_object($properties) ? $properties->length : null;
}
# Fungsi untuk membuat kode automatis
function buatKodefield1($tabel, $inisial){
	$myHost	= "127.0.0.1";
	$myUser	= "root";
	$myPass	= "";
	$myDbs	= "apitexhi_exhibition"; 
	
	# Konek ke Web Server Lokal
	$koneksidb	= mysqli_connect($myHost, $myUser, $myPass, $myDbs);
	if (! $koneksidb) {
		echo "Failed Connection !";
	}

	$struktur	= mysqli_query($koneksidb , "SELECT * FROM $tabel");
	$field		= mysqli_field_name($struktur,1);
	$panjang	= mysqli_field_len($struktur,1);
	echo "<script>console.log( 'Debug panjang: " .  $panjang . "' );</script>";

	 $qry	= mysqli_query(mysqli_connect( "127.0.0.1", "root", "", "apitexhi_exhibition"), "SELECT MAX(".$field.") FROM ".$tabel);
	 echo "<script>console.log( 'Debug field: " .  "SELECT MAX(".$field.") FROM ".$tabel . "' );</script>";
 	$row	= mysqli_fetch_array($qry); 
 	if ($row[0]=="") {
 		$angka=0;
	}
 	else {
 		$angka		= substr($row[0], strlen($inisial));
 	}
	
 	$angka++;
 	$angka	=strval($angka); 
 	$tmp	="";
 	for($i=1; $i<=($panjang-strlen($inisial)-strlen($angka)); $i++) {
		$tmp=$tmp."0";	
	}

	echo "<script>console.log( 'Debug field: " .  $inisial.$tmp.$angka . "' );</script>";
	 return $inisial.$tmp.$angka;
}

function buatKode($tabel, $inisial){
	$myHost	= "127.0.0.1";
	$myUser	= "root";
	$myPass	= "";
	$myDbs	= "apitexhi_exhibition"; 
	
	# Konek ke Web Server Lokal
	$koneksidb	= mysqli_connect($myHost, $myUser, $myPass, $myDbs);
	if (! $koneksidb) {
		echo "Failed Connection !";
	}

	$struktur	= mysqli_query($koneksidb , "SELECT * FROM $tabel");
	$field		= mysqli_field_name($struktur,0);
	$panjang	= mysqli_field_len($struktur,0);
	echo "<script>console.log( 'Debug panjang: " .  $panjang . "' );</script>";

	 $qry	= mysqli_query(mysqli_connect( "127.0.0.1", "root", "", "apitexhi_exhibition"), "SELECT MAX(".$field.") FROM ".$tabel);
	 echo "<script>console.log( 'Debug field: " .  "SELECT MAX(".$field.") FROM ".$tabel . "' );</script>";
 	$row	= mysqli_fetch_array($qry); 
 	if ($row[0]=="") {
 		$angka=0;
	}
 	else {
 		$angka		= substr($row[0], strlen($inisial));
 	}
	
 	$angka++;
 	$angka	=strval($angka); 
 	$tmp	="";
 	for($i=1; $i<=($panjang-strlen($inisial)-strlen($angka)); $i++) {
		$tmp=$tmp."0";	
	}

	echo "<script>console.log( 'Debug field: " .  $inisial.$tmp.$angka . "' );</script>";
	 return $inisial.$tmp.$angka;	 	 
}



function buatKodeById($tabel, $inisial){
	$myHost	= "127.0.0.1";
	$myUser	= "root";
	$myPass	= "";
	$myDbs	= "apitexhi_exhibition"; // nama database, disesuaikan dengan database di MySQL
	
	# Konek ke Web Server Lokal
	$koneksidb	= mysqli_connect($myHost, $myUser, $myPass, $myDbs);
	if (! $koneksidb) {
		echo "Failed Connection !";
	}

	$struktur	= mysqli_query($koneksidb , "SELECT * FROM $tabel");
	$field		= mysqli_field_name($struktur,0);
	$panjang	= mysqli_field_len($struktur,0);

	 $qry	= mysqli_query($koneksidb, "SELECT (id + 1) as id FROM ".$tabel." ORDER BY id DESC LIMIT 1");
	 
 	$row	= mysqli_fetch_array($qry); 
 	// if ($row[0]=="") {
	// 	echo "<script>console.log( 'Debug Objects0: " . $row[0] . "' );</script>";
 	// 	$angka=0;
	// }
 	// else {
	// 	 $angka		= substr($row[0], strlen($inisial));
	// 	 echo "<script>console.log( 'Debug Objects1: " . $angka . "' );</script>";
	// 	 echo "<script>console.log( 'Debug Objects2: " .  strlen($inisial) . "' );</script>";
 	// }
	 $angka=$row[0];
	 echo "<script>console.log( 'Debug Objects1: " .  $angka . "' );</script>";
 	$angka++;
	 echo "<script>console.log( 'Debug Objects2: " .  $angka . "' );</script>";
	 $angka	=strval($angka); 
	 echo "<script>console.log( 'Debug Objects3: " .  $angka . "' );</script>";
 	$tmp	="";
 	for($i=1; $i<=($panjang-strlen($inisial)-strlen($angka)); $i++) {
		$tmp=$tmp."0";	
	}
 	return $inisial.$tmp.$angka;
}

function buatKodeByTime($tabel, $inisial){
	$myHost	= "127.0.0.1";
	$myUser	= "root";
	$myPass	= "";
	$myDbs	= "apitexhi_exhibition"; // nama database, disesuaikan dengan database di MySQL
	
	# Konek ke Web Server Lokal
	$koneksidb	= mysqli_connect($myHost, $myUser, $myPass, $myDbs);
	if (! $koneksidb) {
		echo "Failed Connection !";
	}

	$struktur	= mysqli_query($koneksidb , "SELECT * FROM $tabel");
	$field		= mysqli_field_name($struktur,0);
	$panjang	= mysqli_field_len($struktur,0);

	 $qry	= mysqli_query($koneksidb, "SELECT (id + 1) as id FROM ".$tabel." ORDER BY id DESC LIMIT 1");
	 
 	$row	= mysqli_fetch_array($qry); 
 	// if ($row[0]=="") {
	// 	echo "<script>console.log( 'Debug Objects0: " . $row[0] . "' );</script>";
 	// 	$angka=0;
	// }
 	// else {
	// 	 $angka		= substr($row[0], strlen($inisial));
	// 	 echo "<script>console.log( 'Debug Objects1: " . $angka . "' );</script>";
	// 	 echo "<script>console.log( 'Debug Objects2: " .  strlen($inisial) . "' );</script>";
 	// }
	 $angka=$row[0];
	 echo "<script>console.log( 'Debug Objects1: " .  $angka . "' );</script>";
 	$angka++;
	 echo "<script>console.log( 'Debug Objects2: " .  $angka . "' );</script>";
	 $angka	=strval($angka); 
	 echo "<script>console.log( 'Debug Objects3: " .  $angka . "' );</script>";
 	$tmp	="";
 	for($i=1; $i<=($panjang-strlen($inisial)-strlen($angka)); $i++) {
		$tmp=$tmp."0";	
	}
 	return $inisial.$tmp.$angka;
}

function buatKodePenerimaan($tabel, $inisial){
	$myHost	= "127.0.0.1";
	$myUser	= "root";
	$myPass	= "";
	$myDbs	= "apitexhi_exhibition"; 
	
	# Konek ke Web Server Lokal
	$koneksidb	= mysqli_connect($myHost, $myUser, $myPass, $myDbs);
	if (! $koneksidb) {
		echo "Failed Connection !";
	}

	$struktur	= mysqli_query($koneksidb , "SELECT * FROM $tabel");
	$field		= mysqli_field_name($struktur,3);
	
	$panjang	= mysqli_field_len($struktur,3);
	

	 $qry	= mysqli_query(mysqli_connect( "127.0.0.1", "root", "", "apitexhi_exhibition"), "SELECT MAX(".$field.") FROM ".$tabel);
	
 	$row	= mysqli_fetch_array($qry); 
 	if ($row[0]=="") {
 		$angka=0;
	}
 	else {
 		$angka		= substr($row[0], strlen($inisial));
 	}
	
 	$angka++;
 	$angka	=strval($angka); 
 	$tmp	="";
 	for($i=1; $i<=($panjang-strlen($inisial)-strlen($angka)); $i++) {
		$tmp=$tmp."0";	
	}

	echo "<script>console.log( 'Debug field: " .  $inisial.$tmp.$angka . "' );</script>";
	 return $inisial.$tmp.$angka;
	 
	
	 
}


# Fungsi untuk membuat kode automatis
function buatNoRegistrasi($noRM){
	$myHost	= "127.0.0.1";
	$myUser	= "root";
	$myPass	= "";
	$myDbs	= "apitexhi_exhibition"; // nama database, disesuaikan dengan database di MySQL
	# Konek ke Web Server Lokal
	$koneksidb	= mysqli_connect($myHost, $myUser, $myPass, $myDbs);
	if (! $koneksidb) {
		echo "Failed Connection !";
	}
	$curr_timestamp = date('Ymd');
	//jumlah pasien sudah berkunjung + 1
	$struktur	= mysqli_query($koneksidb , "SELECT count(no_registrasi)+1 as jum_antri FROM pendaftaran WHERE nomor_rm='$noRM'");
	$myData = mysqli_fetch_array($struktur);
	$panjang = sprintf("%03d", $myData['jum_antri']);
	//jumlah pasien pada hari itu
	$jmlPasienQuery	= mysqli_query($koneksidb , "SELECT count(no_registrasi)+1 as jum_pengunjung FROM pendaftaran WHERE tgl_daftar=curdate()");
	$dataJmlPasienQuery = mysqli_fetch_array($jmlPasienQuery);
	$dataJmlPasien = sprintf("%03d", $dataJmlPasienQuery['jum_pengunjung']);

 	return $curr_timestamp.$dataJmlPasien.$panjang ;
}


# Fungsi untuk membalik tanggal dari format Indo (d-m-Y) -> English (Y-m-d)
function InggrisTgl($tanggal){
	$tgl=substr($tanggal,0,2);
	$bln=substr($tanggal,3,2);
	$thn=substr($tanggal,6,4);
	$tanggal="$thn-$bln-$tgl";
	return $tanggal;
}

# Fungsi untuk membalik tanggal dari format English (Y-m-d) -> Indo (d-m-Y)
function IndonesiaTgl($tanggal){
	$tgl=substr($tanggal,8,2);
	$bln=substr($tanggal,5,2);
	$thn=substr($tanggal,0,4);
	$tanggal="$tgl-$bln-$thn";
	return $tanggal;
}

# Fungsi untuk membalik tanggal dari format English (Y-m-d) -> Indo (d-m-Y)
function Indonesia2Tgl($tanggal){
	$namaBln = array("01" => "Januari", "02" => "Februari", "03" => "Maret", "04" => "April", "05" => "Mei", "06" => "Juni", 
					 "07" => "Juli", "08" => "Agustus", "09" => "September", "10" => "Oktober", "11" => "November", "12" => "Desember");
					 
	$tgl=substr($tanggal,8,2);
	$bln=substr($tanggal,5,2);
	$thn=substr($tanggal,0,4);
	$tanggal ="$tgl ".$namaBln[$bln]." $thn";
	return $tanggal;
}

function hitungHari($myDate1, $myDate2){
        $myDate1 = strtotime($myDate1);
        $myDate2 = strtotime($myDate2);
 
        return ($myDate2 - $myDate1)/ (24 *3600);
}

# Fungsi untuk membuat format rupiah pada angka (uang)
function format_angka($angka) {
	$hasil =  number_format($angka,0, ",",".");
	return $hasil;
}

# Fungsi untuk format tanggal, dipakai plugins Callendar
function form_tanggal($nama,$value=''){
	echo" <input type='text' name='$nama' id='$nama' size='11' maxlength='20' value='$value'/>&nbsp;
	<img src='images/calendar-add-icon.png' align='top' style='cursor:pointer; margin-top:7px;' alt='kalender'onclick=\"displayCalendar(document.getElementById('$nama'),'dd-mm-yyyy',this)\"/>			
	";
}

function angkaTerbilang($x){
  $abil = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
  if ($x < 12)
    return " " . $abil[$x];
  elseif ($x < 20)
    return angkaTerbilang($x - 10) . " belas";
  elseif ($x < 100)
    return angkaTerbilang($x / 10) . " puluh" . angkaTerbilang($x % 10);
  elseif ($x < 200)
    return " seratus" . angkaTerbilang($x - 100);
  elseif ($x < 1000)
    return angkaTerbilang($x / 100) . " ratus" . angkaTerbilang($x % 100);
  elseif ($x < 2000)
    return " seribu" . angkaTerbilang($x - 1000);
  elseif ($x < 1000000)
    return angkaTerbilang($x / 1000) . " ribu" . angkaTerbilang($x % 1000);
  elseif ($x < 1000000000)
    return angkaTerbilang($x / 1000000) . " juta" . angkaTerbilang($x % 1000000);
}
?>