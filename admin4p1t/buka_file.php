<?php


# KONTROL MENU PROGRAM
if($_GET) {
	// Jika mendapatkan variabel URL ?page
	switch($_GET['page']){				
		case '' :
			if(!file_exists ("main.php")) die ("Access Denied"); 
			include "main.php";	break;
			
		case 'Halaman-Utama' :
			if(!file_exists ("main.php")) die ("Access Denied"); 
			include "main.php";	break;
		
		case 'Login' :
			if(!file_exists ("login.php")) die ("Access Denied"); 
			include "login.php"; break;
		
		case 'Login-Validasi' :
			if(!file_exists ("login_validasi.php")) die ("Access Denied"); 
			include "login_validasi.php"; break;
			
		case 'Logout' :
			if(!file_exists ("login_out.php")) die ("Access Denied"); 
			include "login_out.php"; break;		
			
		# Kategori
		case 'Kategori-Data' :
			if(!file_exists ("kategori/kategori_data.php")) die ("Access Denied"); 
			include "kategori/kategori_data.php"; break;		
		case 'Kategori-Add' :
			if(!file_exists ("kategori/kategori_add.php")) die ("Access Denied"); 
			include"kategori/kategori_add.php"; break;
		case 'Kategori-Add?status=true' :
			if(!file_exists ("kategori/kategori_add.php")) die ("Access Denied"); 
			include"kategori/kategori_add.php"; break;		
		case 'Kategori-Delete' :
			if(!file_exists ("kategori/kategori_delete.php")) die ("Access Denied"); 
			include "kategori/kategori_delete.php"; break;		
		case 'Kategori-Edit' :
			if(!file_exists ("kategori/kategori_edit.php")) die ("Access Denied"); 
			include "kategori/kategori_edit.php"; break;

		# Kategori-sub
		case 'Kategorisub-Data' :
			if(!file_exists ("kategorisub/kategorisub_data.php")) die ("Access Denied"); 
			include "kategorisub/kategorisub_data.php"; break;		
		case 'Kategorisub-Add' :
			if(!file_exists ("kategorisub/kategorisub_add.php")) die ("Access Denied"); 
			include"kategorisub/kategorisub_add.php"; break;
		case 'Kategorisub-Add?status=true' :
			if(!file_exists ("kategorisub/kategorisub_add.php")) die ("Access Denied"); 
			include"kategorisub/kategorisub_add.php"; break;		
		case 'Kategorisub-Delete' :
			if(!file_exists ("kategorisub/kategorisub_delete.php")) die ("Access Denied"); 
			include "kategorisub/kategorisub_delete.php"; break;		
		case 'Kategorisub-Edit' :
			if(!file_exists ("kategorisub/kategorisub_edit.php")) die ("Access Denied"); 
			include "kategorisub/kategorisub_edit.php"; break;
		
		# Kategori-sub
		case 'Rumah-Data' :
			if(!file_exists ("rumah/rumah_edit.php")) die ("Access Denied"); 
			include "rumah/rumah_edit.php"; break;		
	
		default:
			if(!file_exists ("main.php")) die ("Access Denied"); 
			include "main.php";						
		break;
	}
}
else {
	// Jika tidak mendapatkan variabel URL : ?page
	if(!file_exists ("main.php")) die ("Access Denied"); 
	include "main.php";	
}
?>