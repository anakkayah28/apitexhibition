<?php
include_once "library/inc.seslogin.php";

# UNTUK PAGING (PEMBAGIAN HALAMAN)
$row = 50;
$hal = isset($_GET['hal']) ? $_GET['hal'] : 0;
$pageSql = "SELECT * FROM kategori_sub ORDER BY created_at ASC";
$pageQry = mysqli_query( $koneksidb,$pageSql) or die ("error paging: ".mysql_error());
$jml	 = mysqli_num_rows($pageQry);
$max	 = ceil($jml/$row);
?>

<div class="row">
<div class="col-lg-12">
<h3 class="page-header">Daftar Kategori Sub</h3>
</div>
<!-- /.col-lg-12 -->
</div>

<table style="font-size:12px;" class="table table-striped table-bordered table-hover" id="dataTables-example"  width="100%"cellspacing="1" cellpadding="3">
  <tr>
    <td colspan="2"><a href="?page=Kategorisub-Add" target="_self"><img src="images/btn_tambah_data.png" height="30" border="0" /></a></td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2">
<div class="dataTable-wrapper">
<div class="table-responsive">
<table style="font-size:12px;" class="table table-striped table-bordered table-hover" id="dataTables-example"  width="100%"cellspacing="1" cellpadding="3">
      <tr>
        <th width="10" align="center"><strong>No</strong></th>
        <th  width="10" align="center"><strong>Kategori</strong></th>
        <th  width="10" align="center"><strong>Judul</strong></th>
        <th  width="10" align="center"><strong>Keterangan</strong></th>
        <th  width="10" align="center"><strong>Index</strong></th>
        <th  width="10" align="center"><strong>Status</strong></th>
        <td colspan="2" align="center" bgcolor="#CCCCCC"><strong>Aksi</strong></td>
      </tr>
      <?php
	$mySql = "SELECT * FROM kategori_sub ORDER BY created_at DESC LIMIT $hal, $row";
	$myQry = mysqli_query($koneksidb, $mySql)  or die ("Query salah : ".mysql_error());
	$nomor = 0; 
	while ($myData = mysqli_fetch_array($myQry)) {
		$nomor++;
		$Kode = $myData['id'];
	?>
      <tr>
        <td><?php echo $nomor; ?></td>
        <?php
          	$mySql1 = "SELECT judul FROM kategori WHERE id=". $myData['kategori'];;
            $myQry1 = mysqli_query($koneksidb, $mySql1)  or die ("Query salah : ".mysql_error());
            $myData1 = mysqli_fetch_array($myQry1)
        ?>
        <td><?php echo $myData1['judul']; ?></td>
        <td><?php echo $myData['judul']; ?></td>
        <td><?php echo $myData['keterangan']; ?></td>
        <td  width="10" align="center"><input value="<?php echo $myData['index_lihat']; ?>" width="10" id="txtindex" name="txtindex" /></td>
        <td  width="10" align="center"><?php echo $myData['flag_aktif']; ?></td>
        <td width="10" align="center"><a href="?page=Kategorisub-Edit&Kode=<?php echo $Kode; ?>" target="_self">Edit</a></td>
        <td width="10" align="center"><a href="?page=Kategorisub-Delete&Kode=<?php echo $Kode; ?>" target="_self" alt="Delete Data" onclick="return confirm('ANDA YAKIN AKAN MENGHAPUS INI ... ?')">Delete</a></td>
      </tr>
      <?php } ?>
    </table>
</div></div>
    </td>
  </tr>
  <tr class="selKecil">
    <td width="306"><strong>Jumlah Data :</strong> <?php echo $jml; ?></td>
    <td width="483" align="right"><strong>Halaman ke :</strong>
      <?php
	for ($h = 1; $h <= $max; $h++) {
		$list[$h] = $row * $h - $row;
		echo " <a href='?page=Kategori-Data&hal=$list[$h]'>$h</a> ";
	}
	?></td>
  </tr>
</table>
